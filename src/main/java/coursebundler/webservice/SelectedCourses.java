package coursebundler.webservice;

import java.util.List;

public class SelectedCourses {
    private List<String> selectedCourses;

    public List<String> getSelectedCourses(){ return this.selectedCourses; }
    public void setSelectedCourses(List<String> selectedCourses){ this.selectedCourses = selectedCourses; }

}
